package com.eddieranieri.clienteapi.service;

import com.eddieranieri.clienteapi.model.Cliente;
import com.eddieranieri.clienteapi.repository.ClienteRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {


    private ClienteRepository clienteRepository;

    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public List<Cliente> findAll() {
        return this.clienteRepository.findAll();
    }

    public Cliente save(Cliente novoCliente) {
        //SE ELE TEM ENDEREÇO CADASTRADO
        return this.clienteRepository.save(novoCliente);
    }

    public Cliente findById(Long id) {
        return this.clienteRepository.findById(id).orElse(null);
    }

    public void deleteClient(Long id) {
        this.clienteRepository.deleteById(id);
    }

	public Cliente update(Cliente clienteEditado) {
		// TODO Auto-generated method stub
		return clienteRepository.save(clienteEditado);
	}
}
