package com.eddieranieri.clienteapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eddieranieri.clienteapi.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {


}
