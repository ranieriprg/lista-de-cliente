package com.eddieranieri.clienteapi.controller;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.eddieranieri.clienteapi.model.Cliente;
import com.eddieranieri.clienteapi.service.ClienteService;


@RestController
@RequestMapping("/cliente")
public class ClienteController {

	private ClienteService clienteService;

	public ClienteController(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@RequestMapping("/hello")
	public String sayHello() {
		return "Hey there, just wanna say hello";
	}

	//GET ALL CLIENTS
	@GetMapping
	public ResponseEntity<List<Cliente>> listar() {
		return ResponseEntity.ok(clienteService.findAll());
	}

	@PostMapping()
	public ResponseEntity<Cliente> cadastrar(@RequestBody Cliente cliente) {
		Cliente novoCliente = clienteService.save(cliente);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(novoCliente.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Cliente> filtrar(@PathVariable Long id) {
		return ResponseEntity.ok(clienteService.findById(id));
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Cliente> alterar(@RequestBody Cliente clienteEditado) {
		return ResponseEntity.ok(clienteService.save(clienteEditado));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> alterar(@PathVariable Long id) {
		clienteService.deleteClient(id);
		return ResponseEntity.ok().build();
	}
}
